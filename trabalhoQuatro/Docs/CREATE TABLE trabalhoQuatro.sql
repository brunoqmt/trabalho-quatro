﻿CREATE TABLE pessoaJuridica (
	idPessoaJuridica SERIAL NOT NULL PRIMARY KEY,
	razaoSocial VARCHAR(100),
	nomeFantasia VARCHAR(100),
	informacoes VARCHAR(100)
);

CREATE TABLE categoria (
	idCategoria SERIAL NOT NULL PRIMARY KEY,
	descricao VARCHAR(100)
);

CREATE TABLE marca (
	idMarca SERIAL NOT NULL PRIMARY KEY,
	descricao VARCHAR(100)
);

CREATE TABLE cor (
	idCor SERIAL NOT NULL PRIMARY KEY,
	descricao VARCHAR(100)
);

CREATE TABLE produto (
	idProduto SERIAL NOT NULL PRIMARY KEY,
	descricao VARCHAR(100),
	unidadeMedida INT,
	ncm INT,
	origem VARCHAR(100),
	idFornecedor INT REFERENCES pessoaJuridica(idPessoaJuridica),
	colecao INT,
	idMarca INT REFERENCES marca(idMarca),
	preco DOUBLE PRECISION,
	idCategoria INT REFERENCES categoria(idCategoria),
	idCor INT REFERENCES cor(idCor)
);
	
	
	
	