﻿INSERT INTO pessoaJuridica(razaoSocial, nomeFantasia, informacoes) VALUES ('Razão Social do Fornecedor', 'Nome Fantasia', 'Mais informações detalhadas');
INSERT INTO pessoaJuridica(razaoSocial, nomeFantasia, informacoes) VALUES ('Mais uma Razão Social', 'Outra Fantasia', 'Mais Outras informações detalhadas');

INSERT INTO categoria(descricao) VALUES ('Eletronico');
INSERT INTO categoria(descricao) VALUES ('Alimenticio');

INSERT INTO marca(descricao) VALUES ('Apple');
INSERT INTO marca(descricao) VALUES ('Tio João');

INSERT INTO cor(descricao) VALUES ('Vermelho');
INSERT INTO cor(descricao) VALUES ('Branco');

INSERT INTO produto(descricao, unidadeMedida, ncm, origem, idFornecedor, colecao, idMarca, preco, idCategoria, idCor) VALUES ('iPhone 11', 1, 8963446, 'Industrial', 1, 1, 1, 4000.0, 1, 1);
INSERT INTO produto(descricao, unidadeMedida, ncm, origem, idFornecedor, colecao, idMarca, preco, idCategoria, idCor) VALUES ('Arroz Tio João', 2, 84564566, 'Plantacao', 2, 2, 2, 15.99, 2, 2);