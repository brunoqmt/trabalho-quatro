package br.unisinos.trabalhoQuatro.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.unisinos.trabalhoQuatro.model.bean.caixa.Caixa;
import br.unisinos.trabalhoQuatro.model.util.Global;

/**
 * Classe controller do caixa.
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 19 de nov. de 2021
 */
public class CaixaController {

	public CaixaController() {
	}

	/**
	 * Realiza a abertura do caixa.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 19 de nov. de 2021
	 * @param codigoUsuario
	 * @param valorInicial
	 * @return
	 */
	public Map<String, Object> abrirCaixa(Integer codigoUsuario, Double valorInicial) {
		Caixa caixa = Caixa.getInstance();

		// Verifica se o caixa já não esta aberto.
		if (caixa.isAberto()) {
			return Global.mensagem("Mensagem", "Caixa já aberto.");
		}
		// Valida usuario.
		if (codigoUsuario != 1) {
			return Global.mensagem("Mensagem", "Usuário inválido");
		}

		// Abrir o caixa
		caixa.setHoraAbertura(new Date());
		caixa.setValorAtual(valorInicial);
		caixa.setValorInicial(valorInicial);
		caixa.setCodigoUsuario(codigoUsuario);
		caixa.setAberto(true);

		return infoAbertura();
	}

	/**
	 * Retorna as informações de abertura do caixa.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 19 de nov. de 2021
	 * @return
	 */
	public Map<String, Object> infoAbertura() {
		Caixa caixa = Caixa.getInstance();
		if (!caixa.isAberto()) {
			return Global.mensagem("Mensagem", "Caixa está fechado!");
		}
		Map<String, Object> retornos = new HashMap<>();
		retornos.put("Código Usuario", caixa.getCodigoUsuario());
		retornos.put("Numero Caixa:", caixa.getNumeroCaixa());
		retornos.put("Data Hora Abertura", caixa.getHoraAbertura());
		retornos.put("Valor Inicial", caixa.getValorInicial());
		return retornos;
	}

	/**
	 * Retorna as informações gerais do caixa.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 19 de nov. de 2021
	 * @return
	 */
	public Map<String, Object> infoGeral() {
		Caixa caixa = Caixa.getInstance();
		Map<String, Object> retornos = infoAbertura();
		if (!caixa.isAberto()) {
			return retornos;
		} else {
			retornos.put("Valor Atual no Caixa", caixa.getValorAtual());
			retornos.put("STATUS", (caixa.isAberto() ? "ABERTO" : "FECHADO"));
		}
		return retornos;
	}

	/**
	 * Fechar o caixa.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 20 de nov. de 2021
	 * @param codigoUsuario
	 * @param valorInicial
	 * @return
	 */
	public Map<String, Object> fecharCaixa(Integer codigoUsuario, Double valorRetirada) {
		Caixa caixa = Caixa.getInstance();

		// Verifica se o caixa já não esta aberto.
		if (!caixa.isAberto()) {
			return Global.mensagem("Mensagem", "Caixa já fechado.");
		}
		// Valida usuario.
		if (codigoUsuario != 1) {
			return Global.mensagem("Mensagem", "Usuário inválido");
		}

		Map<String, Object> retornos = null;
		if (valorRetirada != null) {
			retornos = this.fazerSangria(codigoUsuario, valorRetirada);
		} else {
			retornos = new HashMap<>();
		}
		retornos.put("Mensagem", "Fechamento de caixa realizado");
		retornos.put("Valor restante", caixa.getValorAtual());
		caixa.setAberto(false);
		caixa.setHoraAbertura(null);
		caixa.setValorInicial(null);
		caixa.setCodigoUsuario(null);
		return retornos;

	}

	/**
	 * Método responsável por fazer a sangria do caixa
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 20 de nov. de 2021
	 * @param codigoUsuario
	 * @param valor
	 * @return
	 */
	public Map<String, Object> fazerSangria(Integer codigoUsuario, Double valor) {
		Caixa caixa = Caixa.getInstance();

		if (!caixa.isAberto()) {
			return Global.mensagem("Mensagem", "Caixa Fechado.");
		}

		if (caixa.getCodigoUsuario().intValue() != codigoUsuario.intValue()) {
			return Global.mensagem("Mensagem", "Usuário inválido");
		}

		if (valor > caixa.getValorAtual()) {
			return Global.mensagem("Mensagem", "Não há valor suficiente para realizar a sangria.");
		}

		Map<String, Object> retornos = new HashMap<>();
		retornos.put("Valor anterior", caixa.getValorAtual());
		caixa.setValorAtual(caixa.getValorAtual() - valor);
		retornos.put("Mensagem", "Sangria de caixa realizada");
		retornos.put("Valor restante", caixa.getValorAtual());
		retornos.put("Valor removido", valor);
		return retornos;
	}

	/**
	 * Método responsavel por fazer o suprimento do caixa.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 20 de nov. de 2021
	 * @param codigoUsuario
	 * @param valor
	 * @return
	 */
	public Map<String, Object> fazerSuprimento(Integer codigoUsuario, Double valor) {
		Caixa caixa = Caixa.getInstance();

		if (!caixa.isAberto()) {
			return Global.mensagem("Mensagem", "Caixa Fechado.");
		}

		if (caixa.getCodigoUsuario().intValue() != codigoUsuario.intValue()) {
			return Global.mensagem("Mensagem", "Usuário inválido");
		}

		Map<String, Object> retornos = new HashMap<>();
		caixa.setValorAtual(caixa.getValorAtual() + valor);
		retornos.put("Mensagem", "Suprimento de caixa realizado");
		retornos.put("Novo valor", caixa.getValorAtual());
		return retornos;

	}

}
