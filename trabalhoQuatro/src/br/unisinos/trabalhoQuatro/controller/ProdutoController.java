package br.unisinos.trabalhoQuatro.controller;

import java.sql.Connection;

import br.unisinos.trabalhoQuatro.model.bean.Produto;
import br.unisinos.trabalhoQuatro.model.dao.CategoriaDAO;
import br.unisinos.trabalhoQuatro.model.dao.ConnectionDAO;
import br.unisinos.trabalhoQuatro.model.dao.CorDAO;
import br.unisinos.trabalhoQuatro.model.dao.MarcaDAO;
import br.unisinos.trabalhoQuatro.model.dao.PessoaJuridicaDAO;
import br.unisinos.trabalhoQuatro.model.dao.ProdutoDAO;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 17 de out. de 2021
 */
public class ProdutoController {

	private Connection connection;
	private PessoaJuridicaDAO pjDAO;
	private CorDAO corDAO;
	private CategoriaDAO categoriaDAO;
	private MarcaDAO marcaDAO;
	private ProdutoDAO produtoDAO;

	public ProdutoController() {
		connection = new ConnectionDAO().connect();
		pjDAO = new PessoaJuridicaDAO(connection);
		corDAO = new CorDAO(connection);
		categoriaDAO = new CategoriaDAO(connection);
		marcaDAO = new MarcaDAO(connection);
		produtoDAO = new ProdutoDAO(connection);
	}

	public Produto consultarProduto(Integer idProduto) {
		Produto produto = produtoDAO.retrieveByID(idProduto);

		/* Completa informações internas */
		produto.setMarca(marcaDAO.retrieveByID(produto.getMarca().getId()));
		produto.setCor(corDAO.retrieveByID(produto.getCor().getId()));
		produto.setCategoria(categoriaDAO.retrieveByID(produto.getCategoria().getId()));
		produto.setFornecedor(pjDAO.retrieveByID(produto.getFornecedor().getId()));

		return produto;
	}

//	public Produto gerenciarProduto(Produto produto) {)
//		produtoDAO.update(produto);
//		return this.consultarProduto(1);
//	}

	public String gerenciarProduto(Produto produto) {
		Boolean update = produtoDAO.update(produto);
		if (update) {
			return "Produto " + produto.getDescricao() + " atualizado!";
		} else {
			return "Falha ao atualizar Produto";
		}
	}

}
