package br.unisinos.trabalhoQuatro.controller.spring.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.unisinos.trabalhoQuatro.controller.CaixaController;
import br.unisinos.trabalhoQuatro.model.util.Global;

/**
 * Controller Rest de Caixa
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 19 de nov. de 2021
 */
@RestController
public class CaixaRest {

	/**
	 * Endpoint de abertura do caixa
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 20 de nov. de 2021
	 * @param codigoUsuario
	 * @param valorInicial
	 * @return
	 */
	@RequestMapping(value = "/pdv/caixa/abrir", method = RequestMethod.GET, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public @ResponseBody String abrirCaixa(@RequestParam("codigoUsuario") Integer codigoUsuario, @RequestParam("valorInicial") Double valorInicial) {
		CaixaController caixaController = new CaixaController();
		return Global.gson.toJson(caixaController.abrirCaixa(codigoUsuario, valorInicial));
	}

	/**
	 * Endpoint de consulta da abertura do caixa.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 20 de nov. de 2021
	 * @return
	 */
	@RequestMapping(value = "/pdv/caixa/abrir/consultar", method = RequestMethod.GET, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public @ResponseBody String abrirCaixaConsultar() {
		CaixaController caixaController = new CaixaController();
		return Global.gson.toJson(caixaController.infoAbertura());
	}

	/**
	 * Endpoint de consulta geral do caixa.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 20 de nov. de 2021
	 * @return
	 */
	@RequestMapping(value = "/pdv/caixa/consultar", method = RequestMethod.GET, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public @ResponseBody String caixaConsultar() {
		CaixaController caixaController = new CaixaController();
		return Global.gson.toJson(caixaController.infoGeral());
	}

	/**
	 * Endpoint de sangria do caixa.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 20 de nov. de 2021
	 * @param codigoUsuario
	 * @param valor
	 * @return
	 */
	@RequestMapping(value = "/pdv/caixa/sangria", method = RequestMethod.GET, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public @ResponseBody String sangriaCaixa(@RequestParam("codigoUsuario") Integer codigoUsuario, @RequestParam("valor") Double valor) {
		CaixaController caixaController = new CaixaController();
		return Global.gson.toJson(caixaController.fazerSangria(codigoUsuario, valor));
	}

	/**
	 * Endpoint de suprimento do caixa.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 20 de nov. de 2021
	 * @param codigoUsuario
	 * @param valor
	 * @return
	 */
	@RequestMapping(value = "/pdv/caixa/suprimento", method = RequestMethod.GET, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public @ResponseBody String suprimentoCaixa(@RequestParam("codigoUsuario") Integer codigoUsuario, @RequestParam("valor") Double valor) {
		CaixaController caixaController = new CaixaController();
		return Global.gson.toJson(caixaController.fazerSuprimento(codigoUsuario, valor));
	}

	/**
	 * Endpoint de fechar o caixa.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 20 de nov. de 2021
	 * @param codigoUsuario
	 * @param valorRetiradal
	 * @return
	 */
	@RequestMapping(value = "/pdv/caixa/fechar", method = RequestMethod.GET, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public @ResponseBody String fecharCaixa(@RequestParam("codigoUsuario") Integer codigoUsuario, @RequestParam(name = "valorRetirada", required = false) Double valorRetirada) {
		CaixaController caixaController = new CaixaController();
		return Global.gson.toJson(caixaController.fecharCaixa(codigoUsuario, valorRetirada));
	}

}
