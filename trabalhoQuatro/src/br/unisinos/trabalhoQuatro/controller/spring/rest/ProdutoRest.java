package br.unisinos.trabalhoQuatro.controller.spring.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.unisinos.trabalhoQuatro.controller.ProdutoController;
import br.unisinos.trabalhoQuatro.model.bean.Produto;
import br.unisinos.trabalhoQuatro.model.util.Global;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 18 de nov. de 2021
 */
@Controller
public class ProdutoRest {

	@RequestMapping(value = "/teste/bruno", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public @ResponseBody String testeBruno(@RequestHeader("teste") String teste, @RequestBody() String jsonContent) {
		return "Retorno de teste: " + teste + "  |  " + jsonContent;
	}

	@RequestMapping(value = "/pdv/produto/consultar", method = RequestMethod.GET, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public @ResponseBody String produtoConsultar(@RequestHeader("idProduto") String idProduto) {
		ProdutoController produtoController = new ProdutoController();
		Produto produto = produtoController.consultarProduto(Integer.parseInt(idProduto));
		return Global.gson.toJson(produto);
	}

	@RequestMapping(value = "/backoffice/produto/gerenciar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	public @ResponseBody String produtoGerenciar(@RequestBody() String jsonContent) {
		ProdutoController produtoController = new ProdutoController();
		Produto produto = Global.gson.fromJson(jsonContent, Produto.class);
		String retorno = produtoController.gerenciarProduto(produto);
		return retorno;
	}
}
