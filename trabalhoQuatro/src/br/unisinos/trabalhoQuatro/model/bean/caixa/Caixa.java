package br.unisinos.trabalhoQuatro.model.bean.caixa;

import java.util.Date;

public class Caixa {

	private static Caixa instance;

	private boolean aberto;
	private Double valorInicial;
	private Double valorAtual;
	private Integer codigoUsuario;
	private Integer numeroCaixa;
	private Date horaAbertura;

	/**
	 * Construtor privado proibe a criação de forma incorreta de mais uma instancia
	 * desta classe.
	 */
	private Caixa() {
		this.numeroCaixa = 10;
	}

	/**
	 * Método Singleton;
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 21 de nov. de 2021
	 * @return
	 */
	public static synchronized Caixa getInstance() {
		if (instance == null) {
			instance = new Caixa();
		}
		return instance;
	}

	/**
	 * Informa se o caixa esta aberto.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 21 de nov. de 2021
	 * @return
	 */
	public boolean isAberto() {
		return aberto;
	}

	public Double getValorInicial() {
		return valorInicial;
	}

	public Double getValorAtual() {
		return valorAtual;
	}

	public Integer getCodigoUsuario() {
		return codigoUsuario;
	}

	public Integer getNumeroCaixa() {
		return numeroCaixa;
	}

	public void setNumeroCaixa(Integer numeroCaixa) {
		this.numeroCaixa = numeroCaixa;
	}

	public Date getHoraAbertura() {
		return horaAbertura;
	}

	public void setHoraAbertura(Date horaAbertura) {
		this.horaAbertura = horaAbertura;
	}

	public void setAberto(boolean aberto) {
		this.aberto = aberto;
	}

	public void setValorInicial(Double valorInicial) {
		this.valorInicial = valorInicial;
	}

	public void setValorAtual(Double valorAtual) {
		this.valorAtual = valorAtual;
	}

	public void setCodigoUsuario(Integer codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

}
