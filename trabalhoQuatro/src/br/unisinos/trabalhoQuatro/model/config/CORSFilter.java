package br.unisinos.trabalhoQuatro.model.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * Classe de filtro CORS
 * 
 * @author Cristiano Farias <cristiano.farias@multi24h.com.br>
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 */
public class CORSFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		if (response instanceof HttpServletResponse) {
			HttpServletResponse alteredResponse = ((HttpServletResponse) response);
			addCorsHeader(alteredResponse);
		}
		filterChain.doFilter(request, response);
	}

	/**
	 * Metodo de adição dos cabeçalhos de CORS.
	 * 
	 * @param response
	 */
	private void addCorsHeader(HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
		response.addHeader("Access-Control-Allow-Headers", "*");
		response.addHeader("Access-Control-Max-Age", "1728000");
	}

}