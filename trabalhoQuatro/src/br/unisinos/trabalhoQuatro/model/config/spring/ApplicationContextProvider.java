package br.unisinos.trabalhoQuatro.model.config.spring;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Classe que carrega o contexto de inicialização da aplicaçãpara ser usado
 * internamente.
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * 
 * @since 3 de dez. de 2020
 */
public class ApplicationContextProvider implements ApplicationContextAware {

	private static ApplicationContext context;

	public static ApplicationContext getApplicationContext() {
		return context;
	}

	@Override
	public void setApplicationContext(ApplicationContext ac) throws BeansException {
		context = ac;
		try {
//			Start.startServerDeployMode();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static File getFileClassPath(String subPathInClassPath) throws IOException {
		return context.getResource("classpath:" + subPathInClassPath).getFile();
	}

	public static String getContextApplicationName() {
		return context.getApplicationName();
	}

	public static Long getStartTimestamp() {
		return context.getStartupDate();
	}

}
