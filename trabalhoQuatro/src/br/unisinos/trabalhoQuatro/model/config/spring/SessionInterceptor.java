package br.unisinos.trabalhoQuatro.model.config.spring;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/**
 * Classe usada para validação de acesso a todas as requisições de entrada no
 * servidor, Os métodos preHandle e postHandle serão executados antes e depois,
 * respectivamente, da ação (do chamado ao controller). Enquanto o método
 * afterCompletion é chamado no final da requisição, ou seja após ter
 * renderizado o JSP
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 15 de jan de 2019
 * @update Lucas Sommer <lucas.sommer@multi24h.com.br>
 * @when 30 de nov de 2020
 */
public class SessionInterceptor extends HandlerInterceptorAdapter {

////	@Override
////	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object controller) throws Exception {
////		String URI = request.getRequestURI();
//		// tabelas envolvidas
//		// rh_ad_modulo
//		// rh_ad_submodulo
//		// rh_ad_rotas
//		// un_usuarios_acessos
//		// decks
//		// O que estiver no rotas pode ser bloqueado, depende do un_usuarios_acessos
//
////		HttpSession session = request.getSession();
////		PermissaoManagerModulo permissaoManagerModulo = new PermissaoManagerModulo(request);
////		if(MODULO.equals(permissaoManagerModulo.getContext2(uri))) {
////			if(!permissaoManagerModulo.verificaRota()) {
////				response.sendRedirect("/multi24rh/noPermission");
////				return false;
////			}
////		}
////		if(!permissaoManager.processaPermissao(uri)) {
////			response.sendRedirect("/multi24rh/noPermission");
////			return false;
////		}
////		permissaoManager.verificaPermissaoUsuario();
//
//		// Aqui tratativa que impede de ser chamado URLs com final '/'. caso aconteca é
//		// redirecionado para o mesmo endereço retirando o sufixo.
//		if (URI.endsWith("/")) {
//			response.sendRedirect(URI.substring(0, URI.length() - 1));
//			return false;
//		}
//
//		// Aqui vai ser verificado se esta logado. se não redireciona para o login.
//		//
//		//
//		//
//		//
//
//		// Aqui permite a passagem para recursos estaticos
//		if (URI.startsWith(ApplicationContextProvider.getContextApplicationName() + "/resources/")
//				|| URI.startsWith(ApplicationContextProvider.getContextApplicationName() + "/assets/templates/")) {
//			return true;
//		}
//
//		// Aqui permite a passagem para a pagina de sem permissão.
//		if (URI.startsWith(ApplicationContextProvider.getContextApplicationName() + "/noPermission/")) {
//			return true;
//		}
//
//		// Aqui permite a passagem para tela de seleção de entidade
//		if (URI.startsWith(ApplicationContextProvider.getContextApplicationName() + "/ad/entidade")) {
//			return true;
//		}
//
//		// Aqui permite a passagem para webservices externos (sem contexto de entidade)
//		if (URI.startsWith(ApplicationContextProvider.getContextApplicationName() + "/ws/external/")) {
//			return true;
//		}
//
//		// Validação de contexto Entidade
//		if (!ServletUtil.isSelectedEntidade(request)) {
//			// salva a ultima URI caso precise redirecionar depois.
//			// Retorno esta desativado por enquanto.
//			// ServletUtil.setRetornoToRedir(request);
//
//			// Esse code tem comprotamentos diferente para requisições do Navegador e do
//			// Ajax.
//			// Se for via navegador, o Servlet Java redireciona para a 401.jsp que chama a
//			// seleção de entidade.
//			// Se for via ajax, é retornado 401 para o JS do Multi que é configurado no
//			// configs.js, e tb é redirecionado para a seleção de entidade.
//			response.sendError(401);
//			return false;
//		}
//
//		// Aqui permite a passagem para webservices internos (com contexto de entidade)
//		if (URI.startsWith(ApplicationContextProvider.getContextApplicationName() + "/ws/")) {
//			return true;
//		}
//		// Aqui permite a passagem para Módulos web, porêm Verifica se há entidade
//		// válida
//		// selecionada para os modulos.
//		for (Modulos modulo : Modulos.values()) {
//			if (URI.startsWith(ApplicationContextProvider.getContextApplicationName() + modulo.getPath())) {
//				// Verifica se Há Contexto de Entidade selecionado.
////				if (!ServletUtil.isSelectedEntidade(request)) {
////					// salva a ultima URI caso precise redirecionar depois.
////					// Retorno esta desativado por enquanto.
////					// ServletUtil.setRetornoToRedir(request);
////
////					// Esse code tem comprotamentos diferente para requisições do Navegador e do
////					// Ajax.
////					// Se for via navegador, o Servlet Java redireciona para a 401.jsp que chama a
////					// seleção de entidade.
////					// Se for via ajax, é retornado 401 para o JS do Multi que é configurado no
////					// configs.js, e tb é redirecionado para a seleção de entidade.
////					response.sendError(401);
////					return false;
////				}
//				return true;
//			}
//		}
//
////		request.getSession().setAttribute("token",
////				"9ftwCUINdaHDC0ZGvVaW_U2ZgNqkrUrSFfmXQHrezFqY_1g1Cq_fkJfjtsjZ4CgltSE4C9AnK63BnaGpN8T-dC4Gicm6s_QDkx6Q0GgBdK1zSCIQ-jfavTAA2A0iZtYRhFwHtP0f381CEcLMmfbunQfIn6pOFZWqzCH_ta8P5Y8");
////		
//		System.out.println("INTERCEPTOR DE SESSION -> Não foi mapeado para permissão esta requisição: \n\t-> " + URI);
//		response.sendError(404);
//		return false;
//	}
}