package br.unisinos.trabalhoQuatro.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.trabalhoQuatro.model.bean.Categoria;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since  19 de nov. de 2021
 */
public class CategoriaDAO {

	private Connection connection;

	public CategoriaDAO(Connection connection) {
		this.connection = connection;
	}

	public List<Categoria> retrieveAll() {
		List<Categoria> categorias = new ArrayList<>();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM categoria";
			pstmt = connection.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Categoria categoria = new Categoria();
				categoria.setId(rs.getInt("idCategoria"));
				categoria.setDescricao(rs.getString("descricao"));
				/* Add to list */
				categorias.add(categoria);
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve categoria. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return categorias;
	}

	public Categoria retrieveByID(Integer id) {
		Categoria categoria = new Categoria();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM categoria WHERE idCategoria = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				categoria.setId(rs.getInt("idCategoria"));
				categoria.setDescricao(rs.getString("descricao"));
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve categoria by ID. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return categoria;
	}

	public Integer insert(Categoria categoria) {
		String sql = "INSERT INTO categoria(descricao) VALUES (?);";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, categoria.getDescricao());
			pstmt.execute();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Inserting Categoria. Message: " + e.getMessage());
			return null;
		}
	}
	
	public Integer update(Categoria categoria) {
		String sql = "UPDATE categoria SET descricao = ? WHERE idCategoria = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, categoria.getDescricao());
			pstmt.setInt(2,  categoria.getId());
			pstmt.executeUpdate();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Updating Categoria. Message: " + e.getMessage());
			return null;
		}
	}

	public Boolean deleteByID(Integer id) {
		String sql = "DELETE FROM categoria WHERE idCategoria = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			return true;
		} catch (Exception e) {
			System.out.println("#Error: Deleting the Categoria by ID. Message: " + e.getMessage());
			return false;
		}
	}

}
