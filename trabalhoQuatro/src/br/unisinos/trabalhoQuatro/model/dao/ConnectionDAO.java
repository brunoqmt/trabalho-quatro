package br.unisinos.trabalhoQuatro.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe de conexão com o banco de dados. Recebe via parâmetros as informações
 * de conexão.
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since 6 de jul. de 2021
 */
public class ConnectionDAO {

	private String user = "postgres";
	private String pass = "root";
	private String ip = "localhost";
	private String port = "5432";
	private String dbname = "TrabalhoQuatro";

	public ConnectionDAO() {
	}

	public ConnectionDAO(String user, String pass, String ip, String port, String dbname) {
		this.user = user;
		this.pass = pass;
		this.ip = ip;
		this.port = port;
		this.dbname = dbname;
	}

	/**
	 * Method used to establish a connection with database.
	 *
	 * @return Connection to perform operations.
	 */
	public Connection connect() {
		Connection connection = null;
		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://" + ip + ":" + port + "/" + dbname;
			connection = DriverManager.getConnection(url, user, pass);
		} catch (Exception e) {
			System.out.println("#Error: Fail to connect database. Message: " + e.getMessage());
		}
		return connection;
	}

	/**
	 * Method user to finish the connection with database.
	 *
	 * @param connection the database connection to close.
	 */
	public void disconnect(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			System.out.println("#Error: Fail to disconnect the database. Message: " + e.getMessage());
		}
	}

}
