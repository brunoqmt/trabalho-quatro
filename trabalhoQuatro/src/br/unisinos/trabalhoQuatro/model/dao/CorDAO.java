package br.unisinos.trabalhoQuatro.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.trabalhoQuatro.model.bean.Cor;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since  19 de nov. de 2021
 */
public class CorDAO {

	private Connection connection;

	public CorDAO(Connection connection) {
		this.connection = connection;
	}

	public List<Cor> retrieveAll() {
		List<Cor> users = new ArrayList<>();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM cor";
			pstmt = connection.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Cor cor = new Cor();
				cor.setId(rs.getInt("idCor"));
				cor.setDescricao(rs.getString("descricao"));
				/* Add to list */
				users.add(cor);
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve categoria. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return users;
	}

	public Cor retrieveByID(Integer id) {
		Cor cor = new Cor();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM cor WHERE idCor = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				cor.setId(rs.getInt("idCor"));
				cor.setDescricao(rs.getString("descricao"));
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve cor by ID. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return cor;
	}

	public Integer insert(Cor cor) {
		String sql = "INSERT INTO cor(descricao) VALUES (?);";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, cor.getDescricao());
			pstmt.execute();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Inserting Categoria. Message: " + e.getMessage());
			return null;
		}
	}
	
	public Integer update(Cor cor) {
		String sql = "UPDATE cor SET descricao = ? WHERE idCor = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, cor.getDescricao());
			pstmt.setInt(2,  cor.getId());
			pstmt.executeUpdate();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Updating Cor. Message: " + e.getMessage());
			return null;
		}
	}

	public Boolean deleteByID(Integer id) {
		String sql = "DELETE FROM cor WHERE idCor = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			return true;
		} catch (Exception e) {
			System.out.println("#Error: Deleting the Cor by ID. Message: " + e.getMessage());
			return false;
		}
	}

}
