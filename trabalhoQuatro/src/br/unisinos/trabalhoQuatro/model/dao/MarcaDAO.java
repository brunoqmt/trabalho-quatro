package br.unisinos.trabalhoQuatro.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.trabalhoQuatro.model.bean.Marca;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since  19 de nov. de 2021
 */
public class MarcaDAO {

	private Connection connection;

	public MarcaDAO(Connection connection) {
		this.connection = connection;
	}

	public List<Marca> retrieveAll() {
		List<Marca> marcas = new ArrayList<>();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM marca";
			pstmt = connection.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Marca marca = new Marca();
				marca.setId(rs.getInt("idMarca"));
				marca.setDescricao(rs.getString("descricao"));
				/* Add to list */
				marcas.add(marca);
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Marca. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return marcas;
	}

	public Marca retrieveByID(Integer id) {
		Marca marca = new Marca();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM marca WHERE idMarca = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				marca.setId(rs.getInt("idMarca"));
				marca.setDescricao(rs.getString("descricao"));
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Marca by ID. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return marca;
	}

	public Integer insert(Marca marca) {
		String sql = "INSERT INTO marca(descricao) VALUES (?);";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, marca.getDescricao());
			pstmt.execute();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Inserting Marca. Message: " + e.getMessage());
			return null;
		}
	}
	
	public Integer update(Marca marca) {
		String sql = "UPDATE marca SET descricao = ? WHERE idMarca = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, marca.getDescricao());
			pstmt.setInt(2,  marca.getId());
			pstmt.executeUpdate();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Updating Marca. Message: " + e.getMessage());
			return null;
		}
	}

	public Boolean deleteByID(Integer id) {
		String sql = "DELETE FROM marca WHERE idMarca = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			return true;
		} catch (Exception e) {
			System.out.println("#Error: Deleting the Marca by ID. Message: " + e.getMessage());
			return false;
		}
	}

}
