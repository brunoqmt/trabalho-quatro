package br.unisinos.trabalhoQuatro.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.trabalhoQuatro.model.bean.PessoaJuridica;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since  19 de nov. de 2021
 */
public class PessoaJuridicaDAO {

	private Connection connection;

	public PessoaJuridicaDAO(Connection connection) {
		this.connection = connection;
	}

	public List<PessoaJuridica> retrieveAll() {
		List<PessoaJuridica> pessoas = new ArrayList<>();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM pessoaJuridica";
			pstmt = connection.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				PessoaJuridica pessoaJuridica = new PessoaJuridica();
				pessoaJuridica.setId(rs.getInt("idPessoaJuridica"));
				pessoaJuridica.setRazaoSocial(rs.getString("razaoSocial"));
				pessoaJuridica.setNomeFantasia(rs.getString("nomeFantasia"));
				pessoaJuridica.setInformacoes(rs.getString("informacoes"));
				/* Add to list */
				pessoas.add(pessoaJuridica);
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve PessoaJuridica. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return pessoas;
	}

	public PessoaJuridica retrieveByID(Integer id) {
		PessoaJuridica pessoaJuridica = new PessoaJuridica();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM pessoaJuridica WHERE idPessoaJuridica = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				pessoaJuridica.setId(rs.getInt("idPessoaJuridica"));
				pessoaJuridica.setRazaoSocial(rs.getString("razaoSocial"));
				pessoaJuridica.setNomeFantasia(rs.getString("nomeFantasia"));
				pessoaJuridica.setInformacoes(rs.getString("informacoes"));
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve PessoaJuridica by ID. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return pessoaJuridica;
	}

	public Integer insert(PessoaJuridica pessoaJuridica) {
		String sql = "INSERT INTO pessoaJuridica(razaoSocial, nomeFantasia, informacoes) VALUES (?, ?, ?);";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, pessoaJuridica.getRazaoSocial());
			pstmt.setString(2, pessoaJuridica.getNomeFantasia());
			pstmt.setString(3, pessoaJuridica.getInformacoes());
			pstmt.execute();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Inserting PessoaJuridica. Message: " + e.getMessage());
			return null;
		}
	}
	
	public Integer update(PessoaJuridica pessoaJuridica) {
		String sql = "UPDATE pessoaJuridica SET razaoSocial = ?, nomeFantasia = ?, informacoes = ? WHERE idPessoaJuridica = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, pessoaJuridica.getRazaoSocial());
			pstmt.setString(2, pessoaJuridica.getNomeFantasia());
			pstmt.setString(3, pessoaJuridica.getInformacoes());
			pstmt.setInt(4,  pessoaJuridica.getId());
			pstmt.executeUpdate();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Updating PessoaJuridica. Message: " + e.getMessage());
			return null;
		}
	}

	public Boolean deleteByID(Integer id) {
		String sql = "DELETE FROM pessoaJuridica WHERE idPessoaJuridica = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			return true;
		} catch (Exception e) {
			System.out.println("#Error: Deleting the PessoaJuridica by ID. Message: " + e.getMessage());
			return false;
		}
	}

}
