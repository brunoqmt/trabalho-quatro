package br.unisinos.trabalhoQuatro.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.trabalhoQuatro.model.bean.Produto;

/**
 * 
 * @author Bruno Mota Alves <bruno.alves@multi24h.com.br>
 * @since  19 de nov. de 2021
 */
public class ProdutoDAO {

	private Connection connection;

	public ProdutoDAO(Connection connection) {
		this.connection = connection;
	}

	public List<Produto> retrieveAll() {
		List<Produto> produtos = new ArrayList<>();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM produto";
			pstmt = connection.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Produto produto = new Produto();
				produto.setIdProduto(rs.getInt("idProduto"));
				produto.setDescricao(rs.getString("descricao"));
				produto.setUnidadeMedida(rs.getInt("unidadeMedida"));
				produto.setNcm(rs.getInt("ncm"));
				produto.setOrigem(rs.getString("origem"));
				produto.getFornecedor().setId(rs.getInt("idFornecedor"));
				produto.setColecao(rs.getInt("colecao"));
				produto.getMarca().setId(rs.getInt("idMarca"));
				produto.setPreco(rs.getDouble("preco"));
				produto.getCategoria().setId(rs.getInt("idCategoria"));
				produto.getCor().setId(rs.getInt("idCor"));
				/* Add to list */
				produtos.add(produto);
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Produto. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return produtos;
	}

	public Produto retrieveByID(Integer id) {
		Produto produto = new Produto();
		PreparedStatement pstmt = null;
		try {
			String sql = "SELECT * FROM produto WHERE idProduto = ?";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				produto.setIdProduto(rs.getInt("idProduto"));
				produto.setDescricao(rs.getString("descricao"));
				produto.setUnidadeMedida(rs.getInt("unidadeMedida"));
				produto.setNcm(rs.getInt("ncm"));
				produto.setOrigem(rs.getString("origem"));
				produto.getFornecedor().setId(rs.getInt("idFornecedor"));
				produto.setColecao(rs.getInt("colecao"));
				produto.getMarca().setId(rs.getInt("idMarca"));
				produto.setPreco(rs.getDouble("preco"));
				produto.getCategoria().setId(rs.getInt("idCategoria"));
				produto.getCor().setId(rs.getInt("idCor"));
			}
		} catch (Exception e) {
			System.out.println("#Error: Retrieve Produto by ID. Message: " + e.getMessage());
			e.printStackTrace();
		}
		return produto;
	}

	public Integer insert(Produto produto) {
		String sql = "INSERT INTO produto(descricao, unidadeMedida, ncm, origem, idFornecedor, colecao, idMarca, preco, "
				+ "idCategoria, idCor) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, produto.getDescricao());
			pstmt.setInt(2, produto.getUnidadeMedida());
			pstmt.setInt(3, produto.getNcm());
			pstmt.setString(4, produto.getOrigem());
			pstmt.setInt(5, produto.getFornecedor().getId());
			pstmt.setInt(6, produto.getColecao());
			pstmt.setInt(7, produto.getMarca().getId());
			pstmt.setDouble(8, produto.getPreco());
			pstmt.setInt(9, produto.getCategoria().getId());
			pstmt.setInt(10, produto.getCor().getId());
			pstmt.execute();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			System.out.println("#Error: Inserting Produto. Message: " + e.getMessage());
			return null;
		}
	}
	
	public Boolean update(Produto produto) {
		String sql = "UPDATE produto SET descricao = ?, unidadeMedida = ?, ncm = ?, origem = ?, idFornecedor = ?, "
				+ "colecao = ?, idMarca = ?, preco = ?, idCategoria = ?, idCor = ? WHERE idProduto = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, produto.getDescricao());
			pstmt.setInt(2, produto.getUnidadeMedida());
			pstmt.setInt(3, produto.getNcm());
			pstmt.setString(4, produto.getOrigem());
			pstmt.setInt(5, produto.getFornecedor().getId());
			pstmt.setInt(6, produto.getColecao());
			pstmt.setInt(7, produto.getMarca().getId());
			pstmt.setDouble(8, produto.getPreco());
			pstmt.setInt(9, produto.getCategoria().getId());
			pstmt.setInt(10, produto.getCor().getId());
			pstmt.setInt(11, produto.getIdProduto());
			pstmt.executeUpdate();

			return true;
		} catch (Exception e) {
			System.out.println("#Error: Updating Produto. Message: " + e.getMessage());
			return false;
		}
	}

	public Boolean deleteByID(Integer id) {
		String sql = "DELETE FROM produto WHERE idProduto = ?;";
		PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.execute();
			return true;
		} catch (Exception e) {
			System.out.println("#Error: Deleting the Produto by ID. Message: " + e.getMessage());
			return false;
		}
	}

}
