package br.unisinos.trabalhoQuatro.model.util;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Global {

	public static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

	/**
	 * Auxiliar para retorno rapido de map de mensagens.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 21 de nov. de 2021
	 * @param chave
	 * @param valor
	 * @return
	 */
	public static Map<String, Object> mensagem(String chave, Object valor) {
		Map<String, Object> mensagem = new HashMap<String, Object>();
		mensagem.put(chave, valor);
		return mensagem;
	}

}
